package Item_Pack_Instruments__BrickShiftOverwrites
{
	function serverCmdPlantBrick(%client)
	{
		if (%client.phraseKeyTimeout_plant $= "")
			%client.phraseKeyTimeout_plant = 0;

		if (isObject(%player = %client.player) && $Sim::Time - (%client.phraseKeyTimeout_plant) > 0.09)
		{
			if ((%instrument = %player.getMountedImage(0).instrumentType) !$= "" && %client.phraseKey_plant !$= "")
			{
				serverCmdPlayPhrase(%client, %client.phraseKey_plant);
				%client.phraseKeyTimeout_plant = $Sim::Time;
			}
			else
				Parent::serverCmdPlantBrick(%client);
		}
		else
			Parent::serverCmdPlantBrick(%client);
	}

	function serverCmdCancelBrick(%client)
	{
		if (%client.phraseKeyTimeout_cancel $= "")
			%client.phraseKeyTimeout_cancel = 0;
			
		if (isObject(%player = %client.player) && $Sim::Time - (%client.phraseKeyTimeout_cancel) > 0.09)
		{
			if ((%instrument = %player.getMountedImage(0).instrumentType) !$= "" && %client.phraseKey_cancel !$= "")
			{
				serverCmdPlayPhrase(%client, %client.phraseKey_cancel);
				%client.phraseKeyTimeout_cancel = $Sim::Time;
			}
			else
				Parent::serverCmdCancelBrick(%client);
		}
		else
			Parent::serverCmdCancelBrick(%client);
	}

	function serverCmdRotateBrick(%client, %rot)
	{
		if (%client.phraseKeyTimeout_rotateLeft $= "")
			%client.phraseKeyTimeout_rotateLeft = 0;
		
		if (%client.phraseKeyTimeout_rotateRight $= "")
			%client.phraseKeyTimeout_rotateRight = 0;

		if (isObject(%player = %client.player))
		{
			if ((%instrument = %player.getMountedImage(0).instrumentType) !$= "")
			{
				if (%client.phraseKey_rotateLeft !$= "" && %rot < 0 && $Sim::Time - (%client.phraseKeyTimeout_rotateLeft) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_rotateLeft);
					%client.phraseKeyTimeout_rotateLeft = $Sim::Time;
				}
				
				if (%client.phraseKey_rotateRight !$= "" && %rot > 0 && $Sim::Time - (%client.phraseKeyTimeout_rotateRight) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_rotateRight);
					%client.phraseKeyTimeout_rotateRight = $Sim::Time;
				}
			}
			else
				Parent::serverCmdRotateBrick(%client, %rot);
		}
		else
			Parent::serverCmdRotateBrick(%client, %rot);
	}

	function serverCmdShiftBrick(%client, %y, %x, %z)
	{
		if (%client.phraseKeyTimeout_away $= "")
			%client.phraseKeyTimeout_away = 0;
		
		if (%client.phraseKeyTimeout_towards $= "")
			%client.phraseKeyTimeout_towards = 0;

		if (%client.phraseKeyTimeout_left $= "")
			%client.phraseKeyTimeout_left = 0;
		
		if (%client.phraseKeyTimeout_right $= "")
			%client.phraseKeyTimeout_right = 0;

		if (%client.phraseKeyTimeout_up $= "")
			%client.phraseKeyTimeout_up = 0;
		
		if (%client.phraseKeyTimeout_down $= "")
			%client.phraseKeyTimeout_down = 0;

		if (%client.phraseKeyTimeout_thirdUp $= "")
			%client.phraseKeyTimeout_thirdUp = 0;
		
		if (%client.phraseKeyTimeout_thirdDown $= "")
			%client.phraseKeyTimeout_thirdDown = 0;

		if (isObject(%player = %client.player))
		{
			if ((%instrument = %player.getMountedImage(0).instrumentType) !$= "")
			{
				if (%client.phraseKey_away !$= "" && %y > 0 && $Sim::Time - (%client.phraseKeyTimeout_away) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_away);
					%client.phraseKeyTimeout_away = $Sim::Time;
				}
				
				if (%client.phraseKey_towards !$= "" && %y < 0 && $Sim::Time - (%client.phraseKeyTimeout_towards) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_towards);
					%client.phraseKeyTimeout_towards = $Sim::Time;
				}
				
				if (%client.phraseKey_left !$= "" && %x > 0 && $Sim::Time - (%client.phraseKeyTimeout_left) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_left);
					%client.phraseKeyTimeout_left = $Sim::Time;
				}
				
				if (%client.phraseKey_right !$= "" && %x < 0 && $Sim::Time - (%client.phraseKeyTimeout_right) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_right);
					%client.phraseKeyTimeout_right = $Sim::Time;
				}
				
				if (%client.phraseKey_up !$= "" && %z >= 3 && $Sim::Time - (%client.phraseKeyTimeout_up) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_up);
					%client.phraseKeyTimeout_up = $Sim::Time;
				}
				
				if (%client.phraseKey_down !$= "" && %z <= -3 && $Sim::Time - (%client.phraseKeyTimeout_down) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_down);
					%client.phraseKeyTimeout_down = $Sim::Time;
				}
				
				if (%client.phraseKey_thirdUp !$= "" && %z > 0 && %z < 3 && $Sim::Time - (%client.phraseKeyTimeout_thirdUp) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_thirdUp);
					%client.phraseKeyTimeout_thirdUp = $Sim::Time;
				}
				
				if (%client.phraseKey_thirdDown !$= "" && %z < 0 && %z > -3 && $Sim::Time - (%client.phraseKeyTimeout_thirdDown) > 0.09)
				{
					serverCmdPlayPhrase(%client, %client.phraseKey_thirdDown);
					%client.phraseKeyTimeout_thirdDown = $Sim::Time;
				}
			}
			else
				Parent::serverCmdShiftBrick(%client, %y, %x, %z);
		}
		else
			Parent::serverCmdShiftBrick(%client, %y, %x, %z);
	}
};
activatePackage(Item_Pack_Instruments__BrickShiftOverwrites);