function strCount(%string, %needle)
{
	%count = 0;

	for (%i = 0; %i < strLen(%string); %i++)
	{
		if (strPos(getSubStr(%string, %i, strLen(%needle)), %needle) != -1)
		{
			%i += strLen(%needle) - 1;
			%count++;
		}
	}
	return %count;
}

function striCount(%string, %needle)
{
	%count = 0;

	for (%i = 0; %i < strLen(%string); %i++)
	{
		if (striPos(getSubStr(%string, %i, strLen(%needle)), %needle) != -1)
		{
			%i += strLen(%needle) - 1;
			%count++;
		}
	}
	return %count;
}