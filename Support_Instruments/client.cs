//exec("./InstrumentsGui.gui");

function clientCmdReceivePhrase(%phrase)
{
	if (%phrase $= "")
		return;

	if ($InstrumentsGui::ReplacePhrase)
	{
		$InstrumentsGui::PhraseTextbox.setText("");
		$InstrumentsGui::PhraseTextbox.setText(%phrase);
	}
	else
		$InstrumentsGui::PhraseTextbox.setText($InstrumentsGui::PhraseTextbox.getValue() @ "," @ %phrase);

	if ($InstrumentsGui::CopyPhraseToClipboard)
		setClipboard(%phrase);
}

function clientCmdReceiveSong(%song)
{
	if (%song $= "")
		return;

	if ($InstrumentsGui::ReplaceSong)
	{
		$InstrumentsGui::SongTextbox.setText("");
		$InstrumentsGui::SongTextbox.setText(%song);
	}
	else
		$InstrumentsGui::SongTextbox.setText($InstrumentsGui::PhraseTextbox.getValue() @ "," @ %song);

	if ($InstrumentsGui::CopySongToClipboard)
		setClipboard(%song);
}

function InstrumentsClient_ParseHelp() // Ripped from Centhra's Dueling GUI
{
	echo("\n\n\nHELP EDIT\n");

	echo("$dcHelpIdx #1: " @ $dcHelpIdx);
	%l = HelpFileList;
	%count = %l.rowCount();

	%az = "abcdefghijklmnopqrstuvwxyz";
	%helpPath = "Add-Ons/Support_Instruments/help";

	//- Parse Help File Names -
	%c = getFileCount(%helpPath @ "*");
	for(%a = 0; %a < %c; %a++)
	{
		if(%a)
			%f = findNextFile(%helpPath @ "*");
		else
			%f = findFirstFile(%helpPath @ "*");
		%base = fileBase(%f);
		%id = getWord(%base,1);
		if(%id)
			%n[%base] = getSubStr(%az,%id - 1,1) @ "." SPC removeWord(removeWord(%base,0),0);
		else
			%n[%base] = ". Instruments";
 	}

	//- Grab Last Available Index -
	if(!$dcHelpIdx)
	{
		for(%a = 0; %a < %count; %a++)
		{
			%text = %l.getRowText(%a);
			%idx = getWord(%text,0) * 1;
			if(%idx > %highest)
			{
				%highest = %idx;
				echo("HIGHEST: " @ %highest);
			}
		}
		$dcHelpIdx = %highest + 1;
		echo("$dcHelpIdx #2: " @ $dcHelpIdx);
		echo("%highest + 1: " @ %highest + 1);
	}

	//- Rename Help Entries -
	for(%a = 0; %a < %count; %a++)
	{
		%id = %l.getRowID(%a);
		%file = %l.fileName[%id];
		if(filePath(%file) $= %helpPath)
		{
			%name = %n[fileBase(%file)];
			%l.setRowById(%id,$dcHelpIdx @ %name);
			$dcHelpIdx[removeWord(%name,0)] = %id;
		}
	}
	%l.sort(0);
}

package Support_Instruments__client
{
	function HelpDlg::onWake(%this)
	{
		parent::onWake(%this);
		InstrumentsClient_ParseHelp();
	}
};
activatePackage(Support_Instruments__client);