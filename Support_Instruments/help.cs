function serverCmdInstrumentsHelp(%client, %topic)
{
	%path = "Add-Ons/Support_Instruments/help/" @ %topic @ ".txt";

	if (isFile(%path))
	{
		%file = new fileObject();
		%file.openForRead(%path);

		while (!%file.isEoF())
		{
			%line = %file.readLine();

			if (%line $= "")
				%line = " ";

			messageClient(%client, '', %line);
		}
		%file.close();
		%file.delete();
	}
}