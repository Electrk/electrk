//################################################
//##					TO DO: 					##
//##--------------------------------------------##
//## [X] Add chord support                      ##
//## [X] Add song playing 						##
//## [ ] Add Support_Updater support            ##
//################################################

exec("./lib.cs");

if ($Pref::Server::InstrumentChords $= "")
	$Pref::Server::InstrumentChords = true;

if ($Pref::Server::InstrumentsEnabled $= "")
	$Pref::Server::InstrumentsEnabled = true;

if ($Pref::Server::InstrumentPhrasePlaying $= "")
	$Pref::Server::InstrumentPhrasePlaying = true;

if ($Pref::Server::InstrumentPhraseLoadingTimeout $= "")
	$Pref::Server::InstrumentPhraseLoadingTimeout = 5;

if ($Pref::Server::InstrumentPhrasePlayingTimeout $= "")
	$Pref::Server::InstrumentPhrasePlayingTimeout = 1;

if ($Pref::Server::InstrumentPhraseSavingTimeout $= "")
	$Pref::Server::InstrumentPhraseSavingTimeout = 30;

if ($Pref::Server::InstrumentPhraseLoading $= "")
	$Pref::Server::InstrumentPhraseLoading = true;

if ($Pref::Server::InstrumentPhraseSaving $= "")
	$Pref::Server::InstrumentPhraseSaving = true;

if ($Pref::Server::InstrumentSongPlaying $= "")
	$Pref::Server::InstrumentSongPlaying = true;

function Instruments_Init()
{
	//if ($Server::InstrumentNoteCount < 1 || $Server::InstrumentNoteCount $= "")
	//{
		//$Server::InstrumentNoteCount = -1;

	deleteVariables("$Server::Instrument*");

	// The modular system

		for (%i = findFirstFile("Add-Ons/Instrument_*/sounds/*.wav"); %i !$= ""; %i = findNextFile("Add-Ons/Instrument_*/sounds/*.wav"))
		{
			// Check if instrument is enabled

			%addon = strReplace(getSubStr(%i, 8, striPos(%i, "/sounds/") - 8), "-", "DASH");
			%addon = strReplace(%addon, "'", "APOS");

			%addon = $AddOn["__" @ %addon];
			
			if (%addon == -1)
				continue;

			%note = fileBase(%i);
			%note = stripTrailingSpaces(getSubStr(%note, strPos(%note, "_") + 1, strLen(%note))); // Strip the [Instrument]_ part

			//%instrument = striReplace(%i, "Add-Ons/Item_Instrument_", "");
			%instrument = getSubStr(fileBase(%i), 0, strPos(fileBase(%i), "_"));
			%instrument = stripTrailingSpaces(%instrument);

			if ($Server::InstrumentIndex[%instrument] $= "")
			{
				if ($Server::InstrumentCount $= "")
					$Server::InstrumentCount = 1;
				else
					$Server::InstrumentCount++;

				$Server::InstrumentAmount[$Server::InstrumentCount - 1] = %instrument;
				$Server::InstrumentIndex[%instrument] = $ServerInstrumentCount - 1;
			}

			%noteName = %instrument @ "_" @ %note @ "Sound";

			%noteCount = $Server::InstrumentNoteCount["::" @ %instrument];

			if (%noteCount $= "")
				$Server::InstrumentNoteCount["::" @ %instrument] = 0;
			else
				$Server::InstrumentNoteCount["::" @ %instrument]++;

			if (!isObject(%noteName)) // Special thanks to Port for this
			{
				datablock audioProfile(genericInstrumentSound)
				{
					description = "audioClosest3D";
					fileName = %i;
					preload = true;
				};

				if (!isObject(%obj = nameToID("genericInstrumentSound")))
				{
					continue;
				}

				%obj.setName(%noteName);
			}

			//%noteName = getSubStr(%note, strPos(%note, "_") + 1, strLen(%note) - 1);

			$Server::InstrumentNotes["::" @ %instrument @ $Server::InstrumentNoteCount["::" @ %instrument]] = %note;
			$Server::InstrumentNoteAmount["::" @ %instrument @ %note] = $Server::InstrumentNotes["::" @ %instrument];
			$Server::InstrumentTotalNoteCount++;

			// $Server::InstrumentNotes::Guitar[4], $Server::InstrumentNotes::Banjo[12], etc.
			// $Server::InstrumentNoteAmount::Harmonica[5C], $Server::InstrumentNoteAmount::Drums[BD], etc.
		}
	//}
}

Instruments_Init();

//------------------------------------------------------------------------------

function Instruments_PlayNote(%instrument, %pos, %note, %player)
{
	if (!$Pref::Server::InstrumentsEnabled)
		return;

	if (!isObject(%player))
		return;

	if (%player.isDisabled())
		return;

	if (%instrument $= "")
		return;

	if (%instrument !$= %player.client.instrument)
		return;

	//if (!%clickPhrase) // Deprecated feature
	//{
		//if (stripChars(%note, ";/$_-%+") $= "" && %note !$= "")
			//%hasNoNotes = true;

		//if (%hasNoNotes)
			//return;
	//}

	//%note = strReplace(%note, "#", "S");

	%note = stripChars(%note, ";/$_-%");

	if (%note $= "r")
	{
		%note = $Server::InstrumentNotes["::" @ %instrument @ getRandom(0, $Server::InstrumentNoteCount["::" @ %instrument])];
	}

	if (%note $= "")
		return;

	if (strPos(%note, "+") == -1)
	{
		%noteSound1 = %instrument @ "_" @ strReplace(%note, "#", "S") @ "Sound";
	}
	else
	{
		%note = strReplace(%note, "+", "\t");
		%note1 = getField(%note, 0);

		%noteSound1 = %instrument @ "_" @ strReplace(%note1, "#", "S") @ "Sound";

		if ($Pref::Server::InstrumentChords)
		{
			// Purposely hard-coded

			%note2 = getField(%note, 1);
			%note3 = getField(%note, 2);
			%note4 = getField(%note, 3);

			%noteSound2 = %instrument @ "_" @ strReplace(%note2, "#", "S") @ "Sound";
			%noteSound3 = %instrument @ "_" @ strReplace(%note3, "#", "S") @ "Sound";
			%noteSound4 = %instrument @ "_" @ strReplace(%note4, "#", "S") @ "Sound";

			%noteName2 = %note2;
			%noteName3 = %note3;
			%noteName4 = %note4;
		}
	}

	if (%note1 $= "")
		%note1 = %note;

	if (isObject(%noteSound1))
	{
		ServerPlay3d(%noteSound1, %pos);
		%note1 = "\c2" @ %note1;
	}
	else
		%note1 = "\c7" @ %note1;

	if ($Pref::Server::InstrumentChords)
	{
		if (isObject(%noteSound2))
		{
			ServerPlay3d(%noteSound2, %pos);
			%note2 = "\c2" @ %note2;
		}
		else
			%note2 = "\c7" @ %note2;

		if (isObject(%noteSound3) && $Pref::Server::InstrumentChords)
		{
			ServerPlay3d(%noteSound3, %pos);
			%note3 = "\c2" @ %note3;
		}
		else
			%note3 = "\c7" @ %note3;

		if (isObject(%noteSound4) && $Pref::Server::InstrumentChords)
		{
			ServerPlay3d(%noteSound4, %pos);
			%note4 = "\c2" @ %note4;
		}
		else
			%note4 = "\c7" @ %note4;
	}

	%bottomPrint = %note1;

	if (%noteName2 !$= "" && $Pref::Server::InstrumentChords)
	{
		%bottomPrint = %bottomPrint @ "+" @ %note2;
	}

	if (%noteName3 !$= "" && $Pref::Server::InstrumentChords)
	{
		%bottomPrint = %bottomPrint @ "+" @ %note3;
	}

	if (%noteName4 !$= "" && $Pref::Server::InstrumentChords)
	{
		%bottomPrint = %bottomPrint @ "+" @ %note4;
	}

	if (isObject(%player))
	{
		if (%player.getMountedImage(0) !$= "")
		{
			%player.playthread(1, plant);

			if (isObject(%client = %player.client))
			{
				%client.bottomPrint("<font:Palatino Linotype:48>" @ strUpr(%bottomPrint), 1, true);
			}
		}
	}
}

function Instruments_PlayPhrase(%instrument, %player, %phrase, %noteCount, %song, %phraseCount, %tempo, %isLoadedSong)
{
	if (!$Pref::Server::InstrumentsEnabled || !$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (!$Pref::Server::InstrumentSongPlaying && %song)
		return;

	if (!isObject(%player))
		return;

	if (%instrument !$= %player.client.instrument || %player.client.instrument $= "")
		return;

	if (isEventPending(%player.instrumentPhraseSchedule))
		cancel(%player.instrumentPhraseSchedule);

	if (%song !$= "")
	{
		%song = stripChars(%song, "!@#$%^&*()_-=+\\\"'|");

		if (%phraseCount $= "")
			%phraseCount = 0;

		if (isObject(%client = %player.client))
		{
			if (%noteCount >= getWordCount(%phrase) && %phrase !$= "")
			{
				%noteCount = 0;

				%songPhrase = getWord(%song, %phraseCount++);

				if (%isLoadedSong)
					%phrase = strReplace(%client.loadedSongPhrase[%songPhrase], ",", " ");
				else
					%phrase = strReplace(%client.songPhrase[%songPhrase], ",", " ");
			}
			else if (%phrase $= "")
			{
				%songPhrase = getWord(%song, %phraseCount);

				if (%isLoadedSong)
					%phrase = strReplace(%client.loadedSongPhrase[%songPhrase], ",", " ");
				else
					%phrase = strReplace(%client.songPhrase[%songPhrase], ",", " ");
			}
		}
	}

	if (%phrase $= "")
		return;

	if (%noteCount $= "")
		%noteCount = 0;

	if (%tempo $= "" || %tempo < 64 || %tempo > 1000)
		%tempo = 250;

	%note = getWord(%phrase, %noteCount);

	if (%noteCount < getWordCount(%phrase) && %player.client.instrument $= %instrument)
	{
		%player.isPlayingInstrument = true;

		if (%client.lastPhrasePlayed !$= %phrase)
		{
			%client.lastPhrasePlayed = strReplace(%phrase, " ", ",");
		}

		%multiplier = (strCount(%note, ";") * 0.25) + (strCount(%note, "/") * 0.5) + (strCount(%note, "$") * 1)
			+ (strCount(%note, "_") * 2) + (strCount(%note, "-") * 4);

		if (%multiplier <= 0)
			%multiplier = 1;

		%time = %tempo * %multiplier;

		//if (strPos(%note, "/") != -1)
			//%time /= 2;
		//else if (strPos(%note, ";") != -1)
			//%time /= 4;

		//if (strLen(%note) > 1)
			//%note = stripChars(%note, ";/$_-");

		if (getSubStr(%note, 0, 2) $= "t:")
		{
			%tempo = getSubStr(%note, 2, strLen(%note));
			%tempo = stripTrailingSpaces(%tempo);
			%tempo = stripChars(%tempo, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");

			%time = 0;
		}

		if (strPos(%note, "%") != -1)
		{
			%noteCount = -1;

			if (%song !$= "")
				%phraseCount = 0;
		}

		if (%time > 0)
			Instruments_PlayNote(%instrument, %player.getPosition(), stripChars(%note, "%"), %player);

		%player.instrumentPhraseSchedule = schedule(%time, 0, Instruments_PlayPhrase, %instrument, 
			%player, %phrase, %noteCount++, %song, %phraseCount, %tempo, %isLoadedSong);
	}
	else
	{
		cancel(%player.instrumentPhraseSchedule);
		%player.isPlayingInstrument = false;
	}
}

function Instruments_LoadPhrase(%name)
{
	if (!isFile("config/server/savedMusicPhrases/" @ %name @ ".txt"))
		return "";

	%file = new fileObject();

	%file.openForRead("config/server/savedMusicPhrases/" @ %name @ ".txt");

	%loadedPhrase = %file.readLine() TAB %file.readLine();

	%file.close();
	%file.delete();

	return %loadedPhrase;
}

package Support_Instruments
{
	function onServerDestroyed()
	{
		$Server::InstrumentNoteCount = -1;
		Parent::onServerDestroyed();
	}

	function changeGameMode(%gameMode)
	{
		$Server::InstrumentNoteCount = -1;
		Parent::changeGameMode(%gameMode);
	}

	function serverCmdUseTool(%client, %slot)
	{
		Parent::serverCmdUseTool(%client, %slot);

		if (isObject(%player = %client.player))
			%client.instrument = %player.getMountedImage(0).instrumentType;
	}

	function serverCmdUnuseTool(%client)
	{
		Parent::serverCmdUnuseTool(%client);

		if (isObject(%player = %client.player))
			%client.instrument = "";

		if (isEventPending(%player.instrumentPhraseSchedule))
			cancel(%player.instrumentPhraseSchedule);
	}
};
activatePackage(Support_Instruments);

exec("./brickShiftOverwrites.cs");
exec("./commands.cs");