exec("./help.cs");

//////////////
// Phrases  //
//////////////

function serverCmdPlayPhrase(%client, %phrase)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (strLen(%phrase) > 256)
	{
		messageClient(%client, '', "The maximum length for a phrase is 256 characters; yours is" SPC strLen(%phrase));
		return;
	}

	if (isObject(%player = %client.player))
	{
		if (%phrase $= "")
		{
			if (%client.lastPhrasePlayed !$= "")
				%phrase = %client.lastPhrasePlayed;
			else if (%client.loadedPhrase !$= "")
				%phrase = %client.loadedPhrase;
			else
				return;
		}

		if (isObject(%player.getMountedImage(0)))
		{
			if (%client.instrument !$= "")
				%instrumentType = %client.instrument;
			else
				return;
		}
		else
			return;

		cancel(%player.instrumentPhraseSchedule);
		%player.isPlayingInstrument = false;

		Instruments_PlayPhrase(%instrumentType, %player, strReplace(%phrase, ",", " "), 0);
	}
}

function serverCmdPlayLastPhrase(%client, %phrase)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (strLen(%client.lastPhrasePlayed) > 256)
	{
		messageClient(%client, '', "The maximum length for a phrase is 256 characters; yours is" SPC strLen(%client.lastPhrasePlayed));
		return;
	}

	if (isObject(%player = %client.player))
	{
		if (isObject(%player.getMountedImage(0)))
		{
			if (%client.instrument !$= "")
				%instrumentType = %client.instrument;
			else
				return;
		}
		else
			return;

		if (%client.lastPhrasePlayed !$= "")
		{
			cancel(%player.instrumentPhraseSchedule);
			%player.isPlayingInstrument = false;

			Instruments_PlayPhrase(%instrumentType, %player, strReplace(%client.lastPhrasePlayed, ",", " "), 0);
		}
	}
}

function serverCmdPlayLoadPhrase(%client, %name) //, %setLoaded)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (!$Pref::Server::InstrumentPhraseLoading)
		return;

	if (getSimTime() - %client.lastPhraseLoadTime < $Pref::Server::InstrumentPhraseLoadingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastPhraseLoadTime - $Pref::Server::InstrumentPhraseLoadingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to load another phrase.");
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "Please enter the name of the phrase");
		return;
	}

	%client.lastPhraseLoadTime = getSimTime();

	%loadedPhrase = Instruments_LoadPhrase(%name);

	if (%loadedPhrase $= "")
	{
		messageClient(%client, '', "File does not exist");
		%client.loadSuccessful = false;
		return;
	}

	%phrase = getField(%loadedPhrase, 0);

	messageClient(%client, '', "\c6Successfully loaded \c3" @ %name @ "\c6 by: \c2" @ getField(%loadedPhrase, 1));
	serverCmdPlayPhrase(%client, %phrase);

	%client.lastPhrasePlayed = %phrase;
	%client.loadedPhrase = %phrase;
}

//function serverCmdPlaySetLoadPhrase(%client, %name)
//{
	//if (!$Pref::Server::InstrumentPhrasePlaying)
		//return;

	//if (!$Pref::Server::InstrumentPhraseLoading)
		//return;

	//serverCmdPlayLoadPhrase(%client, %name, true);
//}

function serverCmdPlayLoadedPhrase(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (strLen(%client.loadedPhrase) > 256)
	{
		messageClient(%client, '', "The maximum length for a phrase is 256 characters, yours is" SPC strLen(%client.loadedPhrase));
		return;
	}

	if (getSimTime() - %client.lastPhrasePlayedTime < $Pref::Server::InstrumentPhrasePlayingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastPhrasePlayedTime - $Pref::Server::InstrumentPhrasePlayingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to play another phrase.");
		return;
	}

	%client.lastPhrasePlayedTime = getSimTime();

	if (isObject(%player = %client.player))
	{
		if (isObject(%player.getMountedImage(0)))
		{
			if (%client.instrument !$= "")
				%instrumentType = %client.instrument;
			else
				return;
		}
		else
			return;

		if (%client.loadedPhrase !$= "")
		{
			cancel(%player.instrumentPhraseSchedule);
			%player.isPlayingInstrument = false;

			Instruments_PlayPhrase(%instrumentType, %player, strReplace(%client.loadedPhrase, ",", " "), 0);
		}
		else
			messageClient(%client, '', "Please load a phrase first");
	}
}

function serverCmdListPhrases(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	messageClient(%client, '', "\c3Available Phrases:");
	messageClient(%client, '', "");

	while((%i = findNextFile("config/server/savedMusicPhrases/*.txt")) !$= "")
	{
		messageClient(%client, '', "\c6" @ fileBase(%i));
	}
}

function serverCmdSavePhrase(%client, %phrase, %name, %credits)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (!$Pref::Server::InstrumentPhraseSaving)
		return;

	if (getSimTime() - %client.lastPhraseSaveTime < $Pref::Server::InstrumentPhraseSavingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastPhraseSaveTime - $Pref::Server::InstrumentPhraseSavingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to save another phrase.");
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "Please enter a name for your phrase");
		return;
	}

	if (%phrase $= "")
		if (%client.lastPhrasePlayed !$= "")
			%phrase = %client.lastPhrasePlayed;
		else
			return;

	%client.lastPhraseSaveTime = getSimTime();

	%name = stripChars(%name, "/:\\/*");

	if (isFile("config/server/savedMusicPhrases/" @ %name @ ".txt"))
	{
		messageClient(%client, '', "A phrase with that name already exists");
		return;
	}

	if (%credits $= "")
		%credits = %client.name SPC "(" @ %client.getBLID() @ ")";

	%file = new fileObject();

	%file.openForWrite("config/server/savedMusicPhrases/" @ %name @ ".txt");

	%file.writeLine(%phrase);
	%file.writeLine(%credits);

	%file.close();
	%file.delete();

	echo(%client.name SPC "(" @ %client.bl_id @ ") saved phrase:" SPC %name);
	messageClient(%client, '', "\c6Successfully saved phrase as \"" @ %name @ "\"");
}

function serverCmdLoadPhrase(%client, %name)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (!$Pref::Server::InstrumentPhraseLoading)
		return;

	if (getSimTime() - %client.lastPhraseLoadTime < $Pref::Server::InstrumentPhraseLoadingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastPhraseLoadTime - $Pref::Server::InstrumentPhraseLoadingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to load another phrase.");
		%client.loadSuccessful = false;
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "Please enter the name of the phrase");
		%client.loadSuccessful = false;
		return;
	}

	%client.lastPhraseLoadTime = getSimTime();

	%loadedPhrase = Instruments_LoadPhrase(%name);

	if (%loadedPhrase $= "")
	{
		messageClient(%client, '', "File does not exist");
		return;
	}

	%client.loadedPhrase = getField(%loadedPhrase, 0);

	messageClient(%client, '', "\c6Successfully loaded \c3" @ %name @ "\c6 by: \c2" @ getField(%loadedPhrase, 1));

	%client.loadSuccessful = true;
}

//function serverCmdSendPhrase(%client, %target)
//{
	//if (!$Pref::Server::InstrumentPhrasePlaying)
		//return;

	//if (%client.isIgnoringPhraseSends[%target.bl_id])
		//return;

	//if (%client.hasSentPhrase[%target.bl_id])
		//return;

	//%target = findClientByName(%target);

	//if (isObject(%target))
	//{
		
	//}
//}

function serverCmdViewLastPhrase(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (%client.lastPhrasePlayed $= "")
		return;

	%phrase = %client.lastPhrasePlayed;
	%phrase = strReplace(%phrase, ",", " ");

	messageClient(%client, '', " ");
	messageClient(%client, '', "\c3Last Played Phrase:");

	if (getWordCount(%phrase) > 32)
	{
		for(%i = 0; %i < getWordCount(%phrase); %i += 31)
		{
			messageClient(%client, '', "\c6" @ strReplace(getWords(%phrase, %i, %i + 31), " ", ","));
		}
	}
	else
		messageClient(%client, '', "\c6" @ strReplace(%phrase, " ", ","));
}

function serverCmdViewLoadedPhrase(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (%client.loadedPhrase $= "")
		return;

	%phrase = %client.loadedPhrase;
	%phrase = strReplace(%phrase, ",", " ");

	messageClient(%client, '', " ");
	messageClient(%client, '', "\c3Loaded Phrase:");

	if (getWordCount(%phrase) > 32)
	{
		for(%i = 0; %i < getWordCount(%phrase); %i += 31)
		{
			messageClient(%client, '', "\c6" @ strReplace(getWords(%phrase, %i, %i + 31), " ", ","));
		}
	}
	else
		messageClient(%client, '', "\c6" @ strReplace(%phrase, " ", ","));
}

// GUI-Related stuff

function serverCmdRequestPhrase(%client, %phrase)
{
	if (%phrase $= "")
	{
		if (%client.lastPhrasePlayed $= "")
		{
			if (%client.loadedPhrase $= "")
				return;
			else
				%phrase = %client.loadedPhrase;
		}
		else
			%phrase = %client.lastPhrasePlayed;
	}
	commandToClient(%client, 'ReceivePhrase', %phrase);
}

function serverCmdRequestLastPhrase(%client)
{
	if (%client.lastPhrasePlayed $= "")
		return;

	commandToClient(%client, 'ReceivePhrase', %client.lastPhrasePlayed);
}

function serverCmdRequestLoadedPhrase(%client)
{
	if (%client.loadedPhrase $= "")
		return;

	commandToClient(%client, 'ReceivePhrase', %client.loadedPhrase);
}

function serverCmdRequestSong(%client, %song)
{
	if (%song $= "")
	{
		if (%client.lastSongPlayed $= "")
		{
			if (%client.loadedSong $= "")
				return;
			else
				%song = %client.loadedSong;
		}
		else
			%song = %client.lastSongPlayed;
	}
	commandToClient(%client, 'ReceiveSong', %song);
}

function serverCmdRequestLastSong(%client)
{
	if (%client.lastSongPlayed $= "")
		return;

	commandToClient(%client, 'ReceiveSong', %client.lastSongPlayed);
}

function serverCmdRequestLoadedPhrase(%client)
{
	if (%client.loadedSong $= "")
		return;

	commandToClient(%client, 'ReceiveSong', %client.loadedSong);
}

//////////////
//  Songs   //
//////////////

function serverCmdPlaySong(%client, %song)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (isObject(%player = %client.player))
	{
		if (%song $= "")
		{
			if (%client.lastSongPlayed !$= "")
				%song = %client.lastSongPlayed;
			else
				return;
		}
		if (isObject(%player.getMountedImage(0)))
		{
			if (%client.instrument !$= "")
				%instrumentType = %client.instrument;
			else
				return;
		}
		else
			return;

		cancel(%player.instrumentPhraseSchedule);
		%player.isPlayingInstrument = false;

		%client.lastSongPlayed = %song;

		Instruments_PlayPhrase(%instrumentType, %player, "", 0, strReplace(%song, ",", " "), 0);
	}
}

function serverCmdPlayLastSong(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (isObject(%player = %client.player))
	{
		if (%client.lastSongPlayed $= "")
			return;

		if (isObject(%player.getMountedImage(0)))
		{
			if (%player.player.instrument !$= "")
				%instrumentType = %client.instrument;
			else
				return;
		}
		else
			return;

		cancel(%player.instrumentPhraseSchedule);
		%player.isPlayingInstrument = false;

		Instruments_PlayPhrase(%instrumentType, %player, "", 0, strReplace(%client.lastSongPlayed, ",", " "), 0, "", false);
	}
}

function serverCmdPlayLoadSong(%client, %name)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (%name $= "")
		return;

	if (isObject(%player = %client.player))
	{
		serverCmdLoadSong(%client, %name);

		if (!isFile("config/server/savedSongs/" @ %name @ ".txt"))
			return;

		serverCmdPlayLoadedSong(%client);
	}
}

//function serverCmdPlaySetLoadSong(%client, %name)
//{
	//if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		//return;

	//serverCmdPlayLoadSong(%client, %name);
//}

function serverCmdPlayLoadedSong(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (isObject(%player = %client.player))
	{
		%client.lastSongPlayed = %client.loadedSong;

		if (isObject(%player.getMountedImage(0)))
		{
			if (%client.instrument !$= "")
				%instrumentType = %client.instrument;
			else
				return;
		}
		else
			return;

		cancel(%player.instrumentPhraseSchedule);
		%player.isPlayingInstrument = false;

		Instruments_PlayPhrase(%instrumentType, %player, "", 0, strReplace(%client.loadedSong, ",", " "), 0, "", true);
	}
}

function serverCmdSaveSong(%client, %song, %name, %credits)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (!$Pref::Server::InstrumentPhraseSaving)
		return;

	if (getSimTime() - %client.lastPhraseSaveTime < $Pref::Server::InstrumentPhraseSavingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastPhraseSaveTime - $Pref::Server::InstrumentPhraseSavingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to save another song.");
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "Please enter a name for your song");
		return;
	}

	if (%song $= "")
		if (%client.lastSongPlayed !$= "")
			%song = %client.lastSongPlayed;
		else
			return;

	%client.lastPhraseSaveTime = getSimTime();

	%name = stripChars(%name, "/:\\/*");

	if (isFile("config/server/savedSongs/" @ %name @ ".txt"))
	{
		messageClient(%client, '', "A song with that name already exists");
		return;
	}

	if (%credits $= "")
		%credits = %client.name SPC "(" @ %client.getBLID() @ ")";

	%file = new fileObject();

	%file.openForWrite("config/server/savedSongs/" @ %name @ ".txt");

	%file.writeLine(%credits);
	%file.writeLine(%song);

	for(%i = 0; %i < 16; %i++)
		%file.writeLine(%client.songPhrase[%i]);
	
	%file.close();
	%file.delete();

	echo(%client.name SPC "(" @ %client.bl_id @ ") saved song:" SPC %name);
	messageClient(%client, '', "\c6Successfully saved song as \"" @ %name @ "\"");
}

function serverCmdLoadSong(%client, %name)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (!$Pref::Server::InstrumentPhraseLoading)
		return;

	if (getSimTime() - %client.lastPhraseLoadTime < $Pref::Server::InstrumentPhraseLoadingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastPhraseLoadTime - $Pref::Server::InstrumentPhraseLoadingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to load another song.");
		%client.loadSuccessful = false;
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "Please enter the name of the song");
		%client.loadSuccessful = false;
		return;
	}

	%client.lastPhraseLoadTime = getSimTime();

	if (!isFile("config/server/savedSongs/" @ %name @ ".txt"))
	{
		messageClient(%client, '', "File does not exist");
		%client.loadSuccessful = false;
		return;
	}

	%file = new fileObject();

	%file.openForRead("config/server/savedSongs/" @ %name @ ".txt");

	%author = %file.readLine();
	%client.loadedSong = %file.readLine();

	for(%i = 0; %i < 16; %i++)
	{
		%client.loadedSongPhrase[%i] = %file.readLine();
	}

	%file.close();
	%file.delete();

	messageClient(%client, '', "\c6Successfully loaded \c3" @ %name @ "\c6 by: \c2" @ %author);

	%client.loadSuccessful = true;
}

function serverCmdSetSongPhrase(%client, %number, %phrase)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (strLen(%phrase) > 256)
	{
		messageClient(%client, '', "The maximum length for a phrase is 256 characters, yours is" SPC strLen(%phrase));
		return;
	}

	if (%number > 15)
	{
		messageClient(%client, '', "The maximum amount of phrases a song can have is 16");
		return;
	}

	for (%i = 0; %i < 16; %i++)
	{
		%phrase = striReplace(%phrase, "l" @ %i, %client.loadedSongPhrase[%i]);
	}

	%phrase = striReplace(%phrase, "l", %client.loadedPhrase);

	%client.songPhrase[%number] = %phrase;
}

function serverCmdSetLoadSongPhrase(%client, %number, %name)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (!$Pref::Server::InstrumentPhraseLoading)
		return;

	if (getSimTime() - %client.lastPhraseLoadTime < $Pref::Server::InstrumentPhraseLoadingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastPhraseLoadTime - $Pref::Server::InstrumentPhraseLoadingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to load another phrase.");
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "Please enter the name of the phrase");
		return;
	}

	%client.lastPhraseLoadTime = getSimTime();

	%loadedPhrase = Instruments_LoadPhrase(%name);

	if (%loadedPhrase $= "")
	{
		messageClient(%client, '', "File does not exist");
		return;
	}

	messageClient(%client, '', "\c6Successfully loaded \c3" @ %name @ "\c6 by: \c2" @ getField(%loadedPhrase, 1));
	serverCmdSetSongPhrase(%client, %number, getField(%loadedPhrase, 0));
}

function serverCmdSetLoadedSongPhrase(%client, %number, %phrase)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (strLen(%phrase) > 256)
	{
		messageClient(%client, '', "The maximum length for a phrase is 256 characters, yours is" SPC strLen(%phrase));
		return;
	}

	if (%number > 15)
	{
		messageClient(%client, '', "The maximum amount of phrases a song can have is 16");
		return;
	}

	for (%i = 0; %i < 16; %i++)
	{
		%phrase = striReplace(%phrase, "l" @ %i, %client.loadedSongPhrase[%i]);
	}

	%phrase = striReplace(%phrase, "l", %client.loadedPhrase);

	%client.loadedSongPhrase[%number] = %phrase;
}

function serverCmdViewSongPhrases(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	%count = 16;

	for(%i = 0; %i < %count; %i++)
	{
		serverCmdViewSongPhrase(%client, %i);
	}
}

function serverCmdViewLoadedSongPhrases(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	%count = 16;

	for(%i = 0; %i < %count; %i++)
	{
		serverCmdViewLoadedSongPhrase(%client, %i);
	}
	serverCmdViewLoadedSong(%client);
}

function serverCmdViewLoadedSong(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	messageClient(%client, '', " ");
	messageClient(%client, '', "\c3Loaded Song:");
	messageClient(%client, '', "\c6" @ %client.loadedSong);
}

function serverCmdViewSongPhrase(%client, %number)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (%number > 16)
		return;

	%phrase = %client.songPhrase[%number];
	%phrase = strReplace(%phrase, ",", " ");

	if (%phrase $= "")
		return;

	messageClient(%client, '', " ");
	messageClient(%client, '', "\c3Phrase" SPC %number @ ":");

	if (getWordCount(%phrase) > 32)
	{
		for(%i = 0; %i < getWordCount(%phrase); %i += 31)
		{
			messageClient(%client, '', "\c6" @ strReplace(getWords(%phrase, %i, %i + 31), " ", ","));
		}
	}
	else
		messageClient(%client, '', "\c6" @ strReplace(%phrase, " ", ","));
}

function serverCmdListSongs(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	messageClient(%client, '', "\c3Available Songs:");
	messageClient(%client, '', "");

	while((%i = findNextFile("config/server/savedSongs/*.txt")) !$= "")
	{
		messageClient(%client, '', "\c6" @ fileBase(%i));
	}
}

function serverCmdViewLoadedSongPhrase(%client, %number)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (%number > 16)
		return;

	%phrase = %client.loadedSongPhrase[%number];
	%phrase = strReplace(%phrase, ",", " ");

	if (%phrase $= "")
		return;

	messageClient(%client, '', " ");
	messageClient(%client, '', "\c3Loaded Phrase" SPC %number @ ":");

	if (getWordCount(%phrase) > 32)
	{
		for(%i = 0; %i < getWordCount(%phrase); %i += 31)
		{
			messageClient(%client, '', "\c6" @ strReplace(getWords(%phrase, %i, %i + 31), " ", ","));
		}
	}
	else
		messageClient(%client, '', "\c6" @ strReplace(%phrase, " ", ","));
}

////////////////
//  Bindsets  //
////////////////

function serverCmdBindPhraseToKey(%client, %key, %phrase)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	switch$(%key)
	{
		case "plant" or "enter" or "return":
			%client.phraseKey_plant = %phrase;

		case "cancel" or "0":
			%client.phraseKey_cancel = %phrase;

		case "thirdUp" or "3":
			%client.phraseKey_thirdUp = %phrase;

		case "thirdDown" or "1":
			%client.phraseKey_thirdDown = %phrase;

		case "left" or "4":
			%client.phraseKey_left = %phrase;	

		case "right" or "6":
			%client.phraseKey_right = %phrase;

		case "away" or "8":
			%client.phraseKey_away = %phrase;

		case "towards" or "2":
			%client.phraseKey_towards = %phrase;

		case "up" or "+":
			%client.phraseKey_up = %phrase;

		case "down" or "5":
			%client.phraseKey_down = %phrase;

		case "rotateLeft" or "7":
			%client.phraseKey_rotateLeft = %phrase;

		case "rotateRight" or "9":
			%client.phraseKey_rotateRight = %phrase;
	}
}

function serverCmdUnbindAllBindset(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	%client.phraseKey_plant       = "";
	%client.phraseKey_cancel      = "";
	%client.phraseKey_thirdUp     = "";
	%client.phraseKey_thirdDown   = "";
	%client.phraseKey_left        = "";	
	%client.phraseKey_right       = "";
	%client.phraseKey_away        = "";
	%client.phraseKey_towards     = "";
	%client.phraseKey_up          = "";
	%client.phraseKey_down        = "";
	%client.phraseKey_rotateLeft  = "";
	%client.phraseKey_rotateRight = "";
}

function serverCmdSaveBindset(%client, %name, %credits)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (!$Pref::Server::InstrumentPhraseSaving)
		return;

	if (getSimTime() - %client.lastBindsetSaveTime < $Pref::Server::InstrumentPhraseSavingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastBindsetSaveTime - $Pref::Server::InstrumentPhraseSavingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to save another bindset.");
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "\c6Please enter a name.");
		return;
	}

	%client.lastBindsetSaveTime = getSimTime();

	%name = stripChars(%name, "/:\\/*");

	if (isFile("config/server/savedMusicBindsets/" @ %name @ ".txt"))
	{
		messageClient(%client, '', "A bindset with that name already exists");
		return;
	}

	if (%credits $= "")
		%credits = %client.name SPC "(" @ %client.getBLID() @ ")";

	%file = new fileObject();

	%file.openForWrite("config/server/savedMusicBindsets/" @ %name @ ".txt");

	%file.writeLine(%client.phraseKey_plant);
	%file.writeLine(%client.phraseKey_cancel);
	%file.writeLine(%client.phraseKey_thirdUp);
	%file.writeLine(%client.phraseKey_towards);
	%file.writeLine(%client.phraseKey_thirdDown);
	%file.writeLine(%client.phraseKey_left);
	%file.writeLine(%client.phraseKey_down);
	%file.writeLine(%client.phraseKey_right);
	%file.writeLine(%client.phraseKey_rotateLeft);
	%file.writeLine(%client.phraseKey_away);
	%file.writeLine(%client.phraseKey_rotateRight);
	%file.writeLine(%client.phraseKey_up);

	%file.writeLine(%credits);

	%file.close();
	%file.delete();

	echo(%client.name SPC "(" @ %client.bl_id @ ") saved bindset:" SPC %name);
	messageClient(%client, '', "\c6Successfully saved bindset as \c6" @ %name);
}

function serverCmdLoadBindset(%client, %name)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	if (!$Pref::Server::InstrumentPhraseLoading)
		return;

	if (getSimTime() - %client.lastBindsetLoadTime < $Pref::Server::InstrumentPhraseLoadingTimeout * 1000)
	{
		%time = getSimTime() - %client.lastBindsetLoadTime - $Pref::Server::InstrumentPhraseLoadingTimeout * 1000;
		%time = mAbs(mFloatLength(%time / 1000, 0));
		
		if (%time == 1)
			%sec = "second";
		else
			%sec = "seconds";

		messageClient(%client, '', "Please wait" SPC %time SPC %sec SPC "before attempting to load another bindset.");
		return;
	}

	if (%name $= "")
	{
		messageClient(%client, '', "\c6Please enter a name.");
		return;
	}

	%client.lastBindsetLoadTime = getSimTime();

	%name = stripChars(%name, "/:\\/*");

	if (!isFile("config/server/savedMusicBindsets/" @ %name @ ".txt"))
	{
		messageClient(%client, '', "File not found");
		return;
	}

	%file = new fileObject();

	%file.openForRead("config/server/savedMusicBindsets/" @ %name @ ".txt");

	%client.phraseKey_plant = %file.readLine();
	%client.phraseKey_cancel = %file.readLine();
	%client.phraseKey_thirdUp = %file.readLine();
	%client.phraseKey_towards = %file.readLine();
	%client.phraseKey_thirdDown = %file.readLine();
	%client.phraseKey_left = %file.readLine();
	%client.phraseKey_down = %file.readLine();
	%client.phraseKey_right = %file.readLine();
	%client.phraseKey_rotateLeft = %file.readLine();
	%client.phraseKey_away = %file.readLine();
	%client.phraseKey_rotateRight = %file.readLine();
	%client.phraseKey_up = %file.readLine();
	
	%author = %file.readLine();

	%file.close();
	%file.delete();

	echo(%client.name SPC "(" @ %client.bl_id @ ") loaded bindset:" SPC %name);
	messageClient(%client, '', "\c6Successfully loaded \c6" @ %name @ "\c6 by \c2" @ %author);
}

function serverCmdListBindsets(%client)
{
	if (!$Pref::Server::InstrumentPhrasePlaying || !$Pref::Server::InstrumentSongPlaying || !$Pref::Server::InstrumentsEnabled)
		return;

	messageClient(%client, '', "\c3Available Bindsets:");
	messageClient(%client, '', "");

	while((%i = findNextFile("config/server/savedMusicBindsets/*.txt")) !$= "")
	{
		messageClient(%client, '', "\c6" @ fileBase(%i));
	}
}

///////////////////
// Miscellaneous //
///////////////////

function serverCmdStopPlaying(%client)
{
	if (!isObject(%player = %client.player))
		return;

	if (%player.isPlayingInstrument)
	{
		cancel(%player.instrumentPhraseSchedule);
		%player.isPlayingInstrument = false;
	}
}

function serverCmdListInstruments(%client)
{
	if (!$Pref::Server::InstrumentsEnabled)
		return;

	messageClient(%client, '', " ");
	messageClient(%client, '', "\c3Available Instruments:");

	for (%i = $Server::InstrumentCount - 1; %i >= 0; %i--)
	{
		messageClient(%client, '', "\c6" @ $Server::InstrumentAmount[%i]);
	}
}

function serverCmdListNotes(%client, %instrument)
{
	if (!$Pref::Server::InstrumentsEnabled || !$Pref::Server::InstrumentPhrasePlaying)
		return;

	if (%instrument $= "")
	{
		if (isObject(%player = %client.player))
			%instrument = %player.getMountedImage(0).instrumentType;
	}

	if (%instrument !$= "")
	{
		//%instrument = %client.instrument;

		eval("%count = $Server::InstrumentNoteCount::" @ %instrument @ ";");

		if (%count !$= "" && %count != 0)
		{
			messageClient(%client, '', " ");
			messageClient(%client, '', "\c3Available Notes (" @ %instrument @ "):");

			for (%i = %count - 1; %i >= 0; %i--)
			{
				eval("messageClient(%client, '', \"\c6\" @ $Server::InstrumentNotes::" @ %instrument @ "[" @ %i @ "]);");
			}
		}
	}
}

///////////////
// Shortcuts //
///////////////

function serverCmdPP(%client, %phrase)
{
	serverCmdPlayPhrase(%client, %phrase);
}

function serverCmdPS(%client, %song)
{
	serverCmdPlaySong(%client, %song);
}

function serverCmdSP(%client, %phrase, %name)
{
	serverCmdSavePhrase(%client, %phrase, %name);
}

function serverCmdSS(%client, %song, %name)
{
	serverCmdSaveSong(%client, %song, %name);
}

function serverCmdSSP(%client, %song, %phrase)
{
	serverCmdSetSongPhrase(%client, %song, %phrase);
}

function serverCmdPLP(%client, %phrase)
{
	serverCmdPlayLoadPhrase(%client, %phrase);
}

function serverCmdPLS(%client, %song)
{
	serverCmdPlayLoadSong(%client, %song);
}

function serverCmdLBS(%client, %bindset)
{
	serverCmdLoadBindset(%client, %bindset);
}

function serverCmdSBS(%client, %name)
{
	serverCmdSaveBindset(%client, %name);
}

function serverCmdBind(%client, %key, %phrase)
{
	serverCmdBindPhraseToKey(%client, %key, %phrase);
}

function serverCmdUnbindAll(%client)
{
	serverCmdUnbindAllBindset(%client);
}